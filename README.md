- git clone to your pc,
- composer install,
- php artisan key:generate,
- copy .env.example and rename to .env,
- fill credential at .env,
- php artisan migrate seed,
- follow instruction,
- php artisan serve



- http://127.0.0.1:8000/admin/dashboard to go to admin page, if you use admin role